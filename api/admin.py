from django.contrib import admin
from .models import *


# Register your models here.
class LessonAdmin(admin.ModelAdmin):
    pass


class WordAdmin(admin.ModelAdmin):
    pass


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(Lesson, LessonAdmin)
admin.site.register(Word, WordAdmin)
admin.site.register(User, UserAdmin)
