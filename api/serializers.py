from rest_framework import serializers

from .models import Word, Lesson


class NewWord(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = (
            'id',
            'original_word',
        )


class LessonList(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = (
            'id',
            'name'
        )
