from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import Word, Lesson, User
from .serializers import NewWord, LessonList


def root(request):
    return HttpResponse("Hello, Dunya!")


class WordView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, word_id=None, *args, **kwargs):
        if word_id:
            answer = request.query_params.get("answer")
            if not answer:
                return Response({"error": "Please provide answer"})

            word = Word.objects.filter(pk=word_id).first()
            if not word:
                return Response({"error": "Word with such id doesn't exists"})

            if word.translated_word.lower() == answer.lower():
                return Response({"is_answer_right": True})
            else:
                return Response({"is_answer_right": False, "right_answer": word.translated_word})

        else:
            lesson_id = request.query_params.get("lesson_id")
            if not lesson_id:
                return Response({"error": "Please provide lesson"})
            user_id = request.query_params.get("user_id")
            if not user_id:
                return Response({"error": "Please provide user_id"})

            # Bilirem ki bele shey elemezder
            user = User.objects.filter(external_id=user_id).first()
            if not user:
                user = User(external_id=user_id)
                user.save()

            new_word = Word.objects.filter(
                lesson=Lesson.objects.filter(pk=lesson_id).first(),
            ).exclude(fetched_by=user).order_by('?').first()

            if not new_word:
                return Response({
                    "errors": "No words left for you"
                })

            user.fetched_by.add(new_word)

            return Response({
                "word": NewWord(new_word).data
            })


class LessonView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        lessons = Lesson.objects.all()
        return Response({"lessons": LessonList(lessons, many=True).data})
