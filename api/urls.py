from django.urls import path

from .views import *

urlpatterns = [
    path('', root, name='index'),
    path('api/v1/word', WordView.as_view()),
    path('api/v1/word/<int:word_id>', WordView.as_view()),
    path('api/v1/lesson', LessonView.as_view())
]
