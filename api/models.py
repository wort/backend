from django.db import models


# Create your models here.
class User(models.Model):
    external_id = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return f"{self.external_id}"


class Lesson(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)

    def __str__(self):
        return f"{self.name}"


class Word(models.Model):
    original_word = models.CharField(max_length=200, null=False, blank=False)
    translated_word = models.CharField(max_length=200, null=False, blank=False)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    fetched_by = models.ManyToManyField(User, related_name='fetched_by', blank=True)

    def __str__(self):
        return f"original: {self.original_word}; " \
               f"translated: {self.translated_word}; " \
               f"lesson: {self.lesson}"
